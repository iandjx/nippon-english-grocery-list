package com.example.ian.nipponenglishgrocerylist;

/**
 * Created by ian on 23/10/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.ian.nipponenglishgrocerylist.models.Store;

import java.util.ArrayList;

/**
 * - Our StoreAdapter class.
 * - Derives from RecyclerView.Adapter.
 * - Implements android.widget.filterable interface.
 * - We include our MyViewHolder as an inner class.
 * - This adapter layout will be responsible inflating model layout and binding data to resulting views.
 */

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.MyViewHolder> implements Filterable {
    ArrayList<Store> stores;
    ArrayList<Store> currentList;

    public StoreAdapter(ArrayList<Store> stores) {
        this.stores = stores;
        this.currentList = stores;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_store, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.checkBoxStore.setChecked(stores.get(position).isSelected()); //If true checkbox will be checked, else unchecked.
        holder.checkBoxStore.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                stores.get(holder.getAdapterPosition()).setSelected(isChecked);
            }
        });
        holder.tvStoreName.setText(stores.get(position).getName());
        holder.tvStoreAddress.setText(stores.get(position).getAddress());
        holder.tvStoreHours.setText(stores.get(position).getStoreHours());
    }

    @Override
    public int getItemCount() {
        return stores.size();
    }

    public void setStores(ArrayList<Store> filteredStores) {
        this.stores = filteredStores;
    }

    @Override
    public Filter getFilter() {
        return FilterHelper.newInstance(currentList, this);
    }

    /*
    - Our MyViewHolder class
     */
    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvStoreName;
        TextView tvStoreAddress;
        TextView tvStoreHours;
        CheckBox checkBoxStore;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvStoreName = (TextView) itemView.findViewById(R.id.tvStoreName);
            tvStoreAddress = (TextView) itemView.findViewById(R.id.tvStoreAddress);
            tvStoreHours = (TextView) itemView.findViewById(R.id.tvStoreHours);
            checkBoxStore = (CheckBox) itemView.findViewById(R.id.checkBoxStore);
        }
    }

}
