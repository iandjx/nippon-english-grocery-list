package com.example.ian.nipponenglishgrocerylist.models;

/**
 * Created by ian on 23/10/2017.
 */

public class Store {
    private String name;
    private String address;
    private String storeHours;
    private boolean selected;


    public Store(String name, String address, String storeHours) {
        this.name = name;
        this.address = address;
        this.storeHours = storeHours;
        this.selected = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStoreHours() {
        return storeHours;
    }

    public void setStoreHours(String storeHours) {
        this.storeHours = storeHours;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
