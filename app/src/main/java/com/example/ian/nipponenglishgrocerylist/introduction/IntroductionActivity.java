package com.example.ian.nipponenglishgrocerylist.introduction;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import com.example.ian.nipponenglishgrocerylist.MainActivity;
import com.github.paolorotolo.appintro.AppIntro2;

public class IntroductionActivity extends AppIntro2 implements SlideOneFragment.OnFragmentInteractionListener, SlideTwoFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Note here that we DO NOT use setContentView();

        // Add your slide fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.
        addSlide(new SlideOneFragment());
        addSlide(new SlideTwoFragment());
//        addSlide(secondFragment);
//        addSlide(thirdFragment);
//        addSlide(fourthFragment);

        // Instead of fragments, you can also use our default slide
        // Just set a title, description, background and image. AppIntro will do the rest.
//        CharSequence title = "hello";
//        String description = "test";
//        addSlide(AppIntroFragment.newInstance(title, description, R.mipmap.ic_launcher, Color.WHITE));
//        addSlide(AppIntroFragment.newInstance(title, description, R.mipmap.ic_launcher, Color.RED));
//        addSlide(AppIntroFragment.newInstance(title, description, R.mipmap.ic_launcher, Color.YELLOW));


        // OPTIONAL METHODS
        // Override bar/separator color.
//        setBarColor(Color.parseColor("#3F51B5"));
//        setSeparatorColor(Color.parseColor("#2196F3"));

        // Hide Skip/Done button.
        showSkipButton(true);
        setProgressButtonEnabled(true);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
        setVibrate(false);
        setVibrateIntensity(30);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        backToMainActivity();

    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        backToMainActivity();

    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    public void backToMainActivity(){
        Intent intent = new Intent(IntroductionActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
