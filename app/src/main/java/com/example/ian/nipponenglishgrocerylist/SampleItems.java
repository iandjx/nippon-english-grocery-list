package com.example.ian.nipponenglishgrocerylist;

/**
 * Created by ian on 28/10/2017.
 */

public class SampleItems {
    public static String[] englishNames = new String[]{
            "Muntatsu Concentrated Egg 6 Pieces 5 Pack 1 Set (Total: 30)",
            "Egg 40 pieces",
            "Commitment flat-fertile eggs (happy eggs of the day) 10 pieces ※ " +
                    "It is a healthy egg born from a lot of exercise chicken! Normal temperature flight",
            "【Kyoto Wind Sashi】 Canned three-canned egg set 190g × 3 cans / cake roll / soup rolls" +
                    " / egg yaki (20070011 × 3)",
            "Super delicacy egg Poultry delivered directly to delicious taste (30)",
            "Aoi-ya meat of meat! [For business use] Hokkaido chicken peach meat 1 kg " +
                    "(grilled meat grilled meat barbecue BBQ barbecue set)",
            "Yoyogi food mart chicken peach meat Brazilian business frozen thigh meat 2 kg",
            "Kyushu produced young chicken yakitori 50 sets Mushrooms mushrooms " +
                    " Skewered sweet potato skewers 10 each yakitori frozen",
            "beef tangerue sliced barbecue BBQ about 1 kg Mega Sale business freezing",
            "Domestic roast meat block meat about 1000 g (frozen)",
            "Selected Kuroge Wagyu Beef cattle Limited for gifts Peach · Roses Top beef sukiyaki " +
                    "meat 500 g (natural bamboo skin packaging)",
            "Beef Kuroge Wagyu Beef Cuts 1 kg (250 g × 4) Domestic cut off",
            "Beef A4 A5 ranked black hair Wagyu beef cut off sukiyaki yaki shabu-shabu 800 g " +
                    "(400 g × 2) With domestic beef sukiyaki gift ",
            "Kyushu pig sword slash mega prime 1.5 kg (250 g × 6 sets)",
            "About 1 kg of pork belly meat block 【24% price reduction! 】 To pork belly " +
                    "pork belly and sweet vinegar 【Selling agency: The Meat Guy (The Meat Guy)】",
            "Yoyogi Food Mart Pig Rose Block Chilean Pork Belly Block 1 kg",
            "Kyushu pork loin slice mega prime 1 kg (200 g × 5 sets)",
            "【Hinoki meat】 Hida beef & domestic pork entering barbecue set 1kg 【4 ~ 5 people】 frozen"



    };
    public static String[] japaneseNames = new String[]{
            "まんげつ濃厚卵6個入り5パック1セット　（計３０個） [その他]",
            "[訳あり] たまご 40個 (たまごの殻色にムラ 突起やざらつき)",
            "こだわりの平飼い有精卵（たいようの幸せたまご）10個※運動たっぷりの平飼い鶏から生まれた元気な卵です！※常温便",
            "【京風だし】だしまき缶詰3缶セット 190g×3缶/だし巻き/出汁巻き/玉子焼き（20070011x3）",
            "ブログ に 書きたくなる 超濃厚 卵 養鶏場 直送 美味たま（30個）",
            "肉のあおやま メガ盛り！［業務用］ 北海道産 鶏モモ肉 1kg (焼肉 肉 焼き肉 バーベキュー BBQ バーベキューセット)",
            "代々木フードマート 鶏モモ肉 ブラジル産 業務用 冷凍もも肉 2kg",
            "九州産若鶏 焼き鳥50本セット もも串　むね串　ぼんじり串　つくね串　豚ハツ串各10本 やきとり 冷凍",
            "【訳あり】牛タン スライス 焼肉 BBQ 約1kg メガ盛 業務用 冷凍",
            "国産ローストビーフ用ブロック肉　約1000g（冷凍）",
            "厳選 黒毛 和牛 牝牛 限定 ギフト用 モモ・バラ上牛すき焼き肉 500g （ 天然 竹皮 包装 )",
            "牛肉 黒毛和牛 こま切り 1kg(250g×4) こま肉 訳あり 国産 切り落とし",
            "牛肉 A4 A5ランク 黒毛和牛 切り落とし すき焼き 焼きしゃぶ 800g(400g×2) 訳あり 国産 牛肉 すきやき ギフトにも",
            "【訳あり】 九州産 豚こま切れ肉 メガ盛り 1.5kg (250g×6セット)（※北海道・沖縄は配送料要）",
            "豚バラ肉　ブロック　約1kg【24%値下げしました！】豚肉ばら　豚の角煮や甘酢に 【販売元：The Meat Guy(ザ・ミートガイ)】",
            "代々木フードマート 豚バラ ブロック チリ産 豚ばら肉ブロック1kg",
            "九州産 豚ロース スライス メガ盛り 1kg（200g×5セット）",
            "【肉のひぐち】飛騨牛＆国産豚肉入バーベキューセット1ｋｇ【4～5人】冷凍"
    };
    public static int[] resourceIds = new int[]{R.drawable.item1, R.drawable.item2,
            R.drawable.item3, R.drawable.item4, R.drawable.item5, R.drawable.item6,
            R.drawable.item7, R.drawable.item8, R.drawable.item9, R.drawable.item10,
            R.drawable.item11, R.drawable.item12, R.drawable.item13, R.drawable.item14,
            R.drawable.item15, R.drawable.item16, R.drawable.item17, R.drawable.item18};
}
