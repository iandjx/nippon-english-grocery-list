package com.example.ian.nipponenglishgrocerylist.models;

/**
 * Created by ian on 26/10/2017.
 */

public enum ItemCategories {
    PORK,
    BEEF,
    POULTRY,
    VEGETABLE,
    SEASONING,

}
