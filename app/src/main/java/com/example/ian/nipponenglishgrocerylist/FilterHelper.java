package com.example.ian.nipponenglishgrocerylist;

/**
 * Created by ian on 23/10/2017.
 */

import android.widget.Filter;

import com.example.ian.nipponenglishgrocerylist.models.Store;

import java.util.ArrayList;

/**
 * - Our FilterHelper class.
 * - Derives from android.widget.filter.
 * - We perform filtering here and publish results back to the adapter which is then refreshed.
 */

public class FilterHelper extends Filter {
    static ArrayList<Store> currentList;
    static StoreAdapter adapter;

    public static FilterHelper newInstance(ArrayList<Store> currentList, StoreAdapter adapter) {
        FilterHelper.adapter = adapter;
        FilterHelper.currentList = currentList;
        return new FilterHelper();
    }

    /*
    - Perform actual filtering.
     */
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults filterResults = new FilterResults();

        if (constraint != null && constraint.length() > 0) {
            //CHANGE TO UPPER
            constraint = constraint.toString().toUpperCase();

            //HOLD FILTERS WE FIND
            ArrayList<Store> foundFilters = new ArrayList<>();

            String storeName;

            //ITERATE CURRENT LIST
            for (int i = 0; i < currentList.size(); i++) {
                storeName = currentList.get(i).getName();

                //SEARCH
                if (storeName.toUpperCase().contains(constraint)) {
                    //ADD IF FOUND
                    foundFilters.add(currentList.get(i));
                }
            }

            //SET RESULTS TO FILTER LIST
            filterResults.count = foundFilters.size();
            filterResults.values = foundFilters;
        } else {
            //NO ITEM FOUND.LIST REMAINS INTACT
            filterResults.count = currentList.size();
            filterResults.values = currentList;
        }

        //RETURN RESULTS
        return filterResults;
    }

    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

        adapter.setStores((ArrayList<Store>) filterResults.values);
        adapter.notifyDataSetChanged();
    }
}
