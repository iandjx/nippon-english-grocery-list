package com.example.ian.nipponenglishgrocerylist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;

import com.example.ian.nipponenglishgrocerylist.models.Store;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.ArrayList;

public class StoreSearchActivity extends AppCompatActivity {

    private static final int VERTICAL_ITEM_SPACE = 48;
    RecyclerView rv;
    StoreAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //RFERENCE VIEWS
        rv = (RecyclerView) findViewById(R.id.mRecyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));

        rv.addItemDecoration(new DividerItemDecoration(getApplicationContext(), R.drawable.divider));


        MaterialSearchBar searchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
        searchBar.setHint("Search..");
        searchBar.setSpeechMode(true);

        //ADAPTER
        adapter = new StoreAdapter(getStores());
        rv.setAdapter(adapter);

        //SEARCHBAR TEXT CHANGE LISTENER
        searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //SEARCH FILTER
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private ArrayList<Store> getStores() {
        ArrayList<Store> stores = new ArrayList<>();
        Store maxValue = new Store("Max Value", "Sera", "8-5 pm");
        stores.add(maxValue);
        Store pao = new Store("Pao", "Sera", "8-5 pm");
        stores.add(pao);
        Store sevenEleven = new Store("Seven Eleven", "Sera", "8-5 pm");
        stores.add(sevenEleven);
        Store maxValue1 = new Store("Max Value", "Sera", "8-5 pm");
        stores.add(maxValue1);
        Store pao1 = new Store("Pao", "Sera", "8-5 pm");
        stores.add(pao1);
        Store sevenEleven1 = new Store("Seven Eleven", "Sera", "8-5 pm");
        stores.add(sevenEleven1);
        Store maxValue2 = new Store("Max Value", "Sera", "8-5 pm");
        stores.add(maxValue2);
        Store pao2 = new Store("Pao", "Sera", "8-5 pm");
        stores.add(pao2);
        Store sevenEleven2 = new Store("Seven Eleven", "Sera", "8-5 pm");
        stores.add(sevenEleven2);
        Store maxValue3 = new Store("Max Value", "Sera", "8-5 pm");
        stores.add(maxValue3);
        Store pao3 = new Store("Pao", "Sera", "8-5 pm");
        stores.add(pao3);
        Store sevenEleven3 = new Store("Seven Eleven", "Sera", "8-5 pm");
        stores.add(sevenEleven3);
        Store maxValue4 = new Store("Max Value", "Sera", "8-5 pm");
        stores.add(maxValue4);
        Store pao4 = new Store("Pao", "Sera", "8-5 pm");
        stores.add(pao4);
        Store sevenEleven4 = new Store("Seven Eleven", "Sera", "8-5 pm");
        stores.add(sevenEleven4);


        return stores;
    }


}
