package com.example.ian.nipponenglishgrocerylist;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ian.nipponenglishgrocerylist.models.GroceryItem;
import com.example.ian.nipponenglishgrocerylist.models.ItemCategories;

import org.w3c.dom.Text;

public class ItemActivity extends AppCompatActivity {

    TextView tvEnglishName;
    TextView tvJapaneseName;
    TextView tvCategory;
    TextView tvDescription;
    ImageView ivItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);


        GroceryItem pork = new GroceryItem("pork", "ぶたにく","sliced pork", ItemCategories.PORK, R.drawable.pork);

        tvEnglishName = (TextView) findViewById(R.id.tvEnglishName);
        tvJapaneseName = (TextView) findViewById(R.id.tvJapaneseName);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        tvDescription = (TextView) findViewById(R.id.description);
        ivItem = (ImageView) findViewById(R.id.imgItem);


        tvEnglishName.setText(pork.getEnglishNme());
        tvJapaneseName.setText(pork.getJapaneseName());
        tvCategory.setText(pork.getCategory());
        ivItem.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.pork));

    }
}
