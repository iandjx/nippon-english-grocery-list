package com.example.ian.nipponenglishgrocerylist.models;

/**
 * Created by ian on 26/10/2017.
 */

public class GroceryItem {
    private String englishNme;
    private String japaneseName;
    private String description;
    private ItemCategories category;
    private Integer image;

    public GroceryItem(String englishNme, String japaneseName, String description, ItemCategories category, Integer image) {
        this.englishNme = englishNme;
        this.japaneseName = japaneseName;
        this.description = description;
        this.category = category;
        this.image = image;
    }

    public String getEnglishNme() {
        return englishNme;
    }

    public void setEnglishNme(String englishNme) {
        this.englishNme = englishNme;
    }

    public String getJapaneseName() {
        return japaneseName;
    }

    public void setJapaneseName(String japaneseName) {
        this.japaneseName = japaneseName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category.toString();
    }

    public void setCategory(ItemCategories category) {
        this.category = category;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }
}
